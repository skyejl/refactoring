package com.twu.refactoring;

import java.util.List;

public class Order {
    String customName;
    String customAddress;
    List<LineItem> lineItems;

    public Order(String customName, String customAddress, List<LineItem> lineItems) {
        this.customName = customName;
        this.customAddress = customAddress;
        this.lineItems = lineItems;
    }

    public String getCustomerName() {
        return customName;
    }

    public String getCustomerAddress() {
        return customAddress;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }
}
