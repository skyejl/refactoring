package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 */
public class OrderReceipt {
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
    }

    public String printReceipt() {
        StringBuilder output = new StringBuilder();
        getAppend(output, "======Printing Orders======\n");
        getAppend(output, order.getCustomerName());
        getAppend(output, order.getCustomerAddress());

        order.getLineItems().forEach(item -> {
            output.append(item.getDescription()).append('\t').append(item.getPrice()).append('\t')
                    .append(item.getQuantity()).append('\t').append(item.totalAmount()).append('\n');
        });

        double totalSaleTax = 0;
        double taxRate = 0.10;
        double totalSale = 0d;
        totalSaleTax = getTotalSaleTax(totalSaleTax, taxRate);
        totalSale = getTotalSale(taxRate, totalSale);
        getAppend(output, "Sales Tax").append('\t').append(totalSaleTax);
        getAppend(output, "Total Amount").append('\t').append(totalSale);
        return output.toString();
    }

    private double getTotalSale(double taxRate, double totalSale) {
        for (LineItem lineItem : order.getLineItems()) {
            double salesTax = lineItem.totalAmount() * taxRate;
            totalSale += lineItem.totalAmount() + salesTax;
        }
        return totalSale;
    }

    private double getTotalSaleTax(double totalSaleTax, double taxRate) {
        for (LineItem lineItem : order.getLineItems()) {
            double salesTax = lineItem.totalAmount() * taxRate;
            totalSaleTax += salesTax;
        }
        return totalSaleTax;
    }

    private StringBuilder getAppend(StringBuilder output, String strToAppend) {
        return output.append(strToAppend);
    }
}
