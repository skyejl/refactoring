package com.twu.refactoring;

import java.util.ArrayList;
import java.util.Iterator;

public class Customer {

	private String customerName;
	private ArrayList<Rental> rentalList = new ArrayList<Rental>();

	public Customer(String name) {
		this.customerName = name;
	}

	public void addRental(Rental rentalItem) {
		rentalList.add(rentalItem);
	}

	private String getCustomerName() {
		return customerName;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;
		Iterator<Rental> rentals = rentalList.iterator();
		String result = "Rental Record for " + getCustomerName() + "\n";
		while (rentals.hasNext()) {
			Rental rentalItem = rentals.next();

			frequentRenterPoints++;
			frequentRenterPoints = getBonusPoints(frequentRenterPoints, rentalItem);
			result = addRentalList(result, rentalItem);
			totalAmount += getItemAmount(rentalItem);

		}
		result = getFooterLine(totalAmount, frequentRenterPoints, result);
		return result;
	}

	private String addRentalList(String result, Rental rentalItem) {
		result += "\t" + rentalItem.getMovie().getTitle() + "\t"
				+ getItemAmount(rentalItem) + "\n";
		return result;
	}

	private double getItemAmount(Rental rentalItem) {
		double itemAmount = 0;
		switch (rentalItem.getMovie().getPriceCode()) {
		case Movie.REGULAR:
			itemAmount += 2;
			if (rentalItem.getDaysRented() > 2)
				itemAmount += (rentalItem.getDaysRented() - 2) * 1.5;
			break;
		case Movie.NEW_RELEASE:
			itemAmount += rentalItem.getDaysRented() * 3;
			break;
		case Movie.CHILDRENS:
			itemAmount += 1.5;
			if (rentalItem.getDaysRented() > 3)
				itemAmount += (rentalItem.getDaysRented() - 3) * 1.5;
			break;
		}
		return itemAmount;
	}

	private int getBonusPoints(int frequentRenterPoints, Rental rentalItem) {
		if ((rentalItem.getMovie().getPriceCode() == Movie.NEW_RELEASE)
				&& rentalItem.getDaysRented() > 1)
			frequentRenterPoints++;
		return frequentRenterPoints;
	}

	private String getFooterLine(double totalAmount, int frequentRenterPoints, String result) {
		result += ("Amount owed is " + totalAmount + "\n");
		result += String.format("You earned %d frequent renter points", frequentRenterPoints);
		return result;
	}

}
